from apps.globals import DPI, HEIGHT, MAX_DAYS, PLOT_COLOR, WIDTH, Error
from apps.utils import (
    ParamError,
    check_base_parameters,
    check_request,
    check_timeseriesplots_parameters,
    is_valid_nodata,
)


class Parameters:
    def __init__(self):
        self.network = None
        self.station = None
        self.location = None
        self.channel = None
        self.starttime = None
        self.endtime = None
        self.net = "*"
        self.sta = "*"
        self.loc = "*"
        self.cha = "*"
        self.start = None
        self.end = None
        self.iplot = "false"
        self.spectrum = "false"
        self.showtitle = "true"
        self.showscale = "true"
        self.monochrome = "false"
        self.width = WIDTH
        self.height = HEIGHT
        self.dpi = DPI
        self.color = PLOT_COLOR
        self.correct = "false"
        self.earthunits = "false"
        self.demean = "false"
        self.waterlevel = None
        self.freqlimits = None
        self.units = "AUTO"
        self.imageformat = "png"
        self.format = "png"
        self.nodata = "204"
        self.constraints = {
            "alias": [
                ("network", "net"),
                ("station", "sta"),
                ("location", "loc"),
                ("channel", "cha"),
                ("starttime", "start"),
                ("endtime", "end"),
                ("earthunits", "correct"),
            ],
            "booleans": [
                "iplot",
                "spectrum",
                "showtitle",
                "showscale",
                "monochrome",
                "correct",
                "earthunits",
                "demean",
            ],
            "floats": [],
            "not_none": ["start", "end"],
        }

    def todict(self):
        return self.__dict__


def check_parameters(params):
    params = check_base_parameters(params, MAX_DAYS)
    params = check_timeseriesplots_parameters(params)

    # nodata parameter validation
    if not is_valid_nodata(params["nodata"]):
        raise ParamError(Error.NODATA_CODE + str(params["nodata"]))
    params["nodata"] = params["nodata"].lower()

    return params


def checks_get():
    # get default parameters
    params = Parameters().todict()

    # check if the parameters are unknown
    check_request(params)
    params["imageformat"] = params["format"]
    return check_parameters(params)
