import re

from bokeh.embed import file_html
from bokeh.layouts import column
from bokeh.models import DatetimeTickFormatter, HoverTool
from bokeh.plotting import figure
from bokeh.resources import CDN
from flask import make_response

from apps.globals import (
    Error,
)
from apps.utils import (
    ProcessingError,
    get_bounds,
    get_periodogram,
    get_response,
    get_signal_from_client,
    get_units,
    remove_response,
    static_plots,
)

WEBSERVICE = "TimeseriesPlot"


def get_processed_signal(st, params):
    """Signal processing"""

    for tr in st.traces:
        if params["earthunits"] or params["correct"]:
            remove_response(tr, params)
        elif params["demean"]:
            tr.detrend("demean")

    if params["spectrum"]:
        for tr in st.traces:
            tr.time_array, tr.data = get_periodogram(tr.data)

    return st


def date_tick_formatter():
    """Create an xaxis formatter according to different date and time scales."""
    return DatetimeTickFormatter(
        milliseconds="%H:%M:%S.%3N",
        seconds="%H:%M:%S",
        minsec="%H:%M:%S",
        minutes="%H:%M:%S",
        hourmin="%H:%M:%S",
        hours="%H:%M:%S",
        days="%Y/%m/%d",
        months="%Y/%m",
        years="%Y",
    )


def dynamic_plots(params, st):
    """Create dynamic timeseries plots.

    This function return dynamic timeseries plots builds with bokeh and embedded
    into HTML via Flask renderer.

    :param params: parameter dictionary with url parameters (network, station, ...)
    :returns: response_class flask object containing dynamic timeseries plot(s)

    """

    plots = []
    (left, right) = get_bounds(st)
    pcolor = "black" if params["monochrome"] else "#" + params["color"]
    for tr in st:
        # define tools and tooltips
        units = get_units(params, tr.stats.channel)
        tooltips = [("Amplitude", "@y" + units), ("Date", "@x{%F %T.%3N}")]
        hover = HoverTool(tooltips=tooltips, formatters={"@x": "datetime"})
        tools = "crosshair, save, pan, wheel_zoom, box_zoom, zoom_in, zoom_out, reset"

        # create a new plot with the tools above
        plot = figure(
            tools=[hover, tools],
            width=params["width"],
            height=params["height"],
            x_range=(1000 * left, 1000 * right),
            active_drag="box_zoom",
        )
        plot.toolbar.logo = None
        plot.xaxis[0].ticker.desired_num_ticks = 4
        plot.xaxis[0].formatter = date_tick_formatter()
        plot.line([t * 1000 for t in tr.times("timestamp")], tr.data, color=pcolor)
        text = (tr.stats.network, tr.stats.station, tr.stats.location, tr.stats.channel)
        if params["showtitle"]:
            plot.title.text = "[ {}_{}_{}_{} ]".format(*text)
        plots.append(plot)

    # Puts the result in a column.
    plots = column(plots)

    # Generate a complete HTML page embedding the Bokeh plot.
    html = file_html(plots, CDN)
    html = re.sub(r"<title>.*</title>", "<title>resifws-timeseriesplot</title>", html)
    # Returns the rendered HTML to the browser.
    return make_response(html)


def get_output(params):
    """Create timeseries plots.

    :param params: parameter dictionary with url parameters (network, station, ...)
    :returns: static or dynamic plot(s)
    :raises MemoryError raises memory exception
    :raises ValueError raises value exception

    """
    st = get_signal_from_client(params, WEBSERVICE)
    if params["earthunits"] or params["correct"]:
        st.attach_response(get_response(params, WEBSERVICE))
    st = get_processed_signal(st, params)
    st.merge(method=1)

    try:
        if params["iplot"]:
            response = dynamic_plots(params, st)
        else:
            response = static_plots(params, st)
        return response
    except (MemoryError, ValueError) as exception:
        raise ProcessingError(Error.PROCESSING) from exception
    else:
        return response
