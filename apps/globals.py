# global constants
FDSN_CLIENT = "RESIF"
VERSION = "1.1.0"


# limitations
TIMEOUT = 60
MAX_DAYS = 31
MAX_PLOTS = 30
MAX_DATA_POINTS_FAST_MODE = 500000

MAX_POINTS = "100,000,000"
MAX_DATA_POINTS = int(MAX_POINTS.replace(",", ""))

MAX_POINTS_PROCESSING = "10,000,000"
MAX_DATA_POINTS_PROCESSING = int(MAX_POINTS_PROCESSING.replace(",", ""))


# available parameter values
IMAGE_FORMAT = ("png", "jpeg", "jpg")
OUTPUT = ("ascii", "miniseed", "mseed", "plot", "sac", "slist", "tspair")
NODATA_CODE = ("204", "404")
STRING_TRUE = ("yes", "true", "t", "y", "1", "")
STRING_FALSE = ("no", "false", "f", "n", "0")
TAPER_WINDOWS = ("HANNING", "HAMMING", "COSINE")
UNITS = ("AUTO", "VEL", "ACC", "DISP")


# plots constants
PLOT_COLOR = "155084"
XTICKS_ROTATION = 0

HEIGHT = 400
HEIGHT_MIN = 200
HEIGHT_MAX = 1200

WIDTH = 1200
WIDTH_MIN = 200
WIDTH_MAX = 3600

DPI = 100
DPI_MIN = 50
DPI_MAX = 300

# processing constants
WL = 60
WL_MIN = -1000
WL_MAX = 1000

TAPER_MIN = 0.0
TAPER_MAX = 0.5

DECI_MIN = 1
DECI_MAX = 16


class Error:
    UNKNOWN_PARAM = "Unknown query parameter: "
    MULTI_PARAM = "Multiple entries for query parameter: "
    VALID_PARAM = "Valid parameters."
    START_LATER = "The starttime cannot be later than the endtime: "
    TOO_LONG_DURATION = "Too many days requested (greater than "
    TOO_MUCH_DATA = f"The request exceeds the limit of {MAX_POINTS} data points."
    TOO_MUCH_DATA_PROCESSING = (
        f"Deconvolution is not allowed above {MAX_POINTS_PROCESSING} data points."
    )
    RESPONSE = "Deconvolution can't be performed. Instrumental response not available."
    UNSPECIFIED = "Error processing your request."
    NODATA = "Your query doesn't match any available data."
    TIMEOUT = f"Your query exceeds timeout ({TIMEOUT} seconds)."
    MISSING = "Missing parameter: "
    BAD_VAL = " Invalid value: "
    CHAR = "White space(s) or invalid string. Invalid value for: "
    EMPTY = "Empty string. Invalid value for: "
    BOOL = "(Valid boolean values are: true/false, yes/no, t/f or 1/0)"
    NETWORK = "Invalid network code: "
    STATION = "Invalid station code: "
    LOCATION = "Invalid location code: "
    CHANNEL = "Invalid channel code: "
    QUALITY = "Invalid quality code: "
    TIME = "Bad date value: "
    FORMAT = "Invalid image file type: "
    UNITS = "Invalid units type: "
    FREQLIMITS = "Invalid freqlimits value: "
    WATER_EARTHUNITS = (
        "The waterlevel parameter must be used with earthunits=true option."
    )
    INT_BETWEEN = "must be an integer between"
    DPI = f"dpi {INT_BETWEEN} {DPI_MIN} and {DPI_MAX}." + BAD_VAL
    WIDTH = f"width {INT_BETWEEN} {WIDTH_MIN} and {WIDTH_MAX}." + BAD_VAL
    HEIGHT = f"height {INT_BETWEEN} {HEIGHT_MIN} and {HEIGHT_MAX}." + BAD_VAL
    COLOR = "Invalid HEX Color Code (must be such as B22222)" + BAD_VAL
    WATERLEVEL = f"waterlevel must be between {WL_MIN} and {WL_MAX}." + BAD_VAL
    DECI = f"decimate {INT_BETWEEN} {DECI_MIN} and {DECI_MAX}." + BAD_VAL
    TAPER = f"Accepted taper window values are: WIDTH,TYPE\n\
                  WIDTH is between {TAPER_MIN} and {TAPER_MAX}\n\
                  TYPE is HANNING, HAMMING or COSINE.\n\
                  Invalid taper value: "
    BP = "Invalid band pass filter value: "
    SCALE_DIVSCALE = "You cannot specify both scale and divscale values."
    OUTPUT_TIMESERIES = f"Accepted format values are: {OUTPUT}." + BAD_VAL
    NODATA_CODE = f"Accepted nodata values are: {NODATA_CODE}." + BAD_VAL
    PLOTS = (
        f"The request exceeds the limit of {MAX_PLOTS} plots. "
        "Try to reduce the number of channels requested "
        "(network, station, location and channel parameters)."
    )
    PROCESSING = "Your request cannot be processed. Check for value consistency."
    MISSING_OUTPUT = "Missing format parameter. "
    NO_SELECTION = "Request contains no selections."
    NO_WILDCARDS = (
        "Wildcards or lists are allowed only with plot "
        "or mseed output options (Invalid value for: "
    )
