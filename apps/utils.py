import datetime
import io
import os
import platform
import re
import sys
from difflib import SequenceMatcher

import matplotlib.pyplot as plt
import numpy as np
import sentry_sdk
from flask import Response, make_response, request
from matplotlib.ticker import FuncFormatter, MaxNLocator
from obspy import __version__ as obspy_version
from obspy.clients.fdsn import Client
from obspy.clients.fdsn.header import platform_
from obspy.core import UTCDateTime
from scipy.signal import periodogram
from werkzeug.http import HTTP_STATUS_CODES

from apps.globals import (
    DPI,
    DPI_MAX,
    DPI_MIN,
    HEIGHT_MAX,
    HEIGHT_MIN,
    IMAGE_FORMAT,
    MAX_DATA_POINTS,
    MAX_DATA_POINTS_FAST_MODE,
    MAX_DATA_POINTS_PROCESSING,
    MAX_PLOTS,
    NODATA_CODE,
    OUTPUT,
    STRING_FALSE,
    STRING_TRUE,
    TAPER_WINDOWS,
    UNITS,
    VERSION,
    WIDTH_MAX,
    WIDTH_MIN,
    WL,
    WL_MAX,
    WL_MIN,
    XTICKS_ROTATION,
    Error,
    FDSN_CLIENT,
)


class ProcessingError(Exception):
    status_code = 413


class ParamError(Exception):
    status_code = 400


def initialize_sentry():
    sentry_sdk.init(
        dsn=os.environ.get("SENTRY_DSN", ""),
        environment=os.environ.get("SENTRY_ENVIRONMENT", "development"),
        traces_sample_rate=1.0,
        profiles_sample_rate=float(os.environ.get("SENTRY_PROFILES_SAMPLE_RATE", 0.01)),
    )


def get_obspy_client(webservice) -> Client:
    user_agent = (
        f"{webservice} "
        f"(ObsPy/{obspy_version}, {platform_}, "
        f"Python {platform.python_version()})"
    )
    return Client(FDSN_CLIENT, user_agent=user_agent)


def is_valid_integer(dimension, mini=0, maxi=sys.maxsize):
    # by default valid for positive integers
    try:
        dimension = int(dimension)
    except (ValueError, TypeError):
        return False
    return bool(mini <= dimension <= maxi)


def is_valid_float(dimension, mini=sys.float_info.epsilon, maxi=sys.float_info.max):
    # by default valid for strictly positive floats
    try:
        dimension = float(dimension)
    except (ValueError, TypeError):
        return False
    return bool(mini <= dimension <= maxi)


def is_valid_datetime(date):
    for df in ("%Y-%m-%d", "%Y-%m-%dT%H:%M:%S", "%Y-%m-%dT%H:%M:%S.%f"):
        try:
            return datetime.datetime.strptime(date.replace("Z", ""), df)
        except (ValueError, TypeError):
            pass


def is_valid_starttime(date):
    return is_valid_datetime(date) or date == "currentutcday"


def is_valid_endtime(date):
    return is_valid_datetime(date) or date == "currentutcday" or is_valid_integer(date)


def is_valid_network(network):
    return re.match("[A-Za-z0-9*?]{1,2}$", network) if network else False


def is_valid_station(station):
    return re.match("[A-Za-z0-9*?]{1,5}$", station) if station else False


def is_valid_location(location):
    return re.match("([A-Za-z0-9*?-]{1,2})$", location) if location else False


def is_valid_channel(channel):
    return re.match("([A-Za-z0-9*?]{1,3})$", channel) if channel else False


def is_valid_bool_string(string):
    if string is None:
        return False
    return bool(string.lower() in STRING_TRUE + STRING_FALSE)


def is_valid_output(output):
    return output.lower() in OUTPUT if output else False


def is_valid_quality(quality):
    return re.match("[DMQR*?]{1}$", quality) if quality else False


def is_valid_image_format(image):
    # None or empty are false
    return image.lower() in IMAGE_FORMAT if image else False


def is_valid_units(units):
    return units.upper() in UNITS if units else False


def is_valid_taper_window(taper):
    return is_valid_float(taper[0], 0.0, 0.5) and (
        len(taper) == 1 or (len(taper) == 2 and taper[1].upper() in TAPER_WINDOWS)
    )


def is_valid_bpfilter(bp):
    return len(bp) == 2 and all(is_valid_float(b) for b in bp)


def is_valid_freqlimits(freqs):
    return len(freqs) == 4 and all(is_valid_float(f, 0.0) for f in freqs)


def is_valid_color(color):
    return re.match(r"^(?:[0-9a-fA-F]{2}){3}$", color) if color else False


def is_valid_nodata(nodata):
    return nodata.lower() in NODATA_CODE if nodata else False


def currentutcday():
    return datetime.datetime.now(tz=datetime.UTC).replace(
        hour=0, minute=0, second=0, microsecond=0
    )


def handle_exception(exception, request):
    now = datetime.datetime.now(tz=datetime.UTC).strftime("%Y-%b-%d %H:%M:%S UTC")
    match exception.status_code:
        case None:
            code = 424
        case 204:
            code = 404 if request.args.get("nodata") == "404" else 204
        case 500 | 501 | 502 | 503 | 504:
            code = 424
        case _:
            code = exception.status_code
    message = (
        f"Error {code}: {HTTP_STATUS_CODES[code]}\n\n"
        f"{exception!s}\n\n"
        f"Request:\n{request.full_path}\n\n"
        f"Request Submitted:\n{now}\n\n"
        f"Service version:\nversion: {VERSION}"
    )
    return Response(message, status=code, mimetype="text/plain")


def check_request(params):
    # preliminary parameter checks

    for key, val in request.args.items():
        # stops at the first unknown parameter meet:
        if key not in params:
            ratios = ((SequenceMatcher(None, key, p).ratio(), p) for p in params)
            guess = max(ratios)
            hint = ". Did you mean " + guess[1] + " ?" if guess[0] > 0.7 else ""
            raise ParamError(Error.UNKNOWN_PARAM + key + hint)

        # find nonword chars except :
        # "," for lists "*?" for wildcards and ".:-" for date
        if re.search(r"[^a-zA-Z0-9_,*?.:-]", val):
            raise ParamError(Error.CHAR + key)

    for key, val in params.items():
        if len(request.args.getlist(key)) > 1:
            raise ParamError(Error.MULTI_PARAM + key)
        params[key] = request.args.get(key, val)

    for key, alias in params["constraints"]["alias"]:
        if key in request.args and alias in request.args:
            msg = f"{Error.MULTI_PARAM}{key} (and is shorthand {alias})"
            raise ParamError(msg)
        if params[key] is None or params[key] == "false":
            params[key] = request.args.get(alias, params[alias])
        else:
            params[alias] = params[key]
    params["spectrum"] = "false"  # disable spectrum


def check_base_parameters(params, max_days=None):
    # Search for missing mandatory parameters
    for key in params["constraints"]["not_none"]:
        if params[key] is None:
            raise ParamError(Error.MISSING + key)

    # Boolean parameter validations
    for key in params["constraints"]["booleans"]:
        val = params[key]
        if not is_valid_bool_string(val):
            msg = f"Invalid {key} value: {val} {Error.BOOL}."
            raise ParamError(msg)
        params[key] = bool(val.lower() in STRING_TRUE)

    # Float parameter validations
    for key in params["constraints"]["floats"]:
        val = params[key]
        if is_valid_float(val):
            params[key] = float(val)
        elif val is not None:
            msg = f"Invalid {key} value: {val}"
            raise ParamError(msg)

    # Station validations
    network = params["network"].split(",")
    station = params["station"].split(",")
    location = params["location"].split(",")
    channel = params["channel"].split(",")

    for net in network:
        if not is_valid_network(net):
            raise ParamError(Error.NETWORK + net)
    for sta in station:
        if not is_valid_station(sta):
            raise ParamError(Error.STATION + sta)
    for loc in location:
        if not is_valid_location(loc):
            raise ParamError(Error.LOCATION + loc)
    for cha in channel:
        if not is_valid_channel(cha):
            raise ParamError(Error.CHANNEL + cha)

    # Start time and end time validations
    if params["start"] is not None:
        if not is_valid_starttime(params["start"]):
            raise ParamError(Error.TIME + str(params["start"]))

        if is_valid_datetime(params["start"]):
            params["start"] = is_valid_datetime(params["start"])
        elif params["start"] == "currentutcday":
            params["start"] = currentutcday()

    if params["end"] is not None:
        if not is_valid_endtime(params["end"]):
            raise ParamError(Error.TIME + str(params["end"]))

        if is_valid_datetime(params["end"]):
            params["end"] = is_valid_datetime(params["end"])
        elif params["end"] == "currentutcday":
            params["end"] = currentutcday()

    if params["start"] is not None and params["end"] is not None:
        if is_valid_integer(params["start"]) and is_valid_integer(params["end"]):
            params["start"] = currentutcday() - datetime.timedelta(
                seconds=int(params["start"])
            )
            params["end"] = currentutcday() + datetime.timedelta(
                seconds=int(params["end"])
            )

        if is_valid_integer(params["end"]):
            params["end"] = params["start"] + datetime.timedelta(
                seconds=int(params["end"])
            )

        if params["start"] > params["end"]:
            start = str(params["start"])
            end = str(params["end"])
            raise ParamError(Error.START_LATER + start + " > " + end)

        if max_days and params["end"] - params["start"] > datetime.timedelta(
            days=max_days
        ):
            raise ParamError(Error.TOO_LONG_DURATION + f"{max_days} days).")

    # Search for empty selection
    if params["network"] == params["station"] == params["channel"] == "*":
        raise ParamError(Error.NO_SELECTION)
    return params


def check_timeseriesplots_parameters(params):
    # Plot parameter validations
    if not is_valid_integer(params["width"], WIDTH_MIN, WIDTH_MAX):
        raise ParamError(Error.WIDTH + str(params["width"]))
    if not is_valid_integer(params["height"], HEIGHT_MIN, HEIGHT_MAX):
        raise ParamError(Error.HEIGHT + str(params["height"]))
    if not is_valid_integer(params["dpi"], DPI_MIN, DPI_MAX):
        raise ParamError(Error.DPI + str(params["dpi"]))
    if not is_valid_color(params["color"]):
        raise ParamError(Error.COLOR + str(params["color"]))

    params["width"] = int(params["width"])
    params["height"] = int(params["height"])
    params["dpi"] = int(params["dpi"])

    # Water level parameter validation
    if params["waterlevel"] is not None and not params["earthunits"]:
        raise ParamError(Error.WATER_EARTHUNITS)

    if params["waterlevel"] is None:
        params["waterlevel"] = float(WL)
    elif is_valid_float(params["waterlevel"], WL_MIN, WL_MAX):
        params["waterlevel"] = float(params["waterlevel"])
    elif params["waterlevel"].lower() == "none":
        params["waterlevel"] = None
    else:
        raise ParamError(Error.WATERLEVEL + str(params["waterlevel"]))

    # Freqlimits validation (deconvolution prefilter)
    if params["freqlimits"] is not None:
        params["freqlimits"] = tuple(re.split("[/,;-]", params["freqlimits"]))
        if not is_valid_freqlimits(params["freqlimits"]):
            raise ParamError(Error.FREQLIMITS + str(params["freqlimits"]))
        params["freqlimits"] = [float(params["freqlimits"][i]) for i in range(4)]

    # Units validation
    if not is_valid_units(params["units"]):
        raise ParamError(Error.UNITS + str(params["units"]))
    params["units"] = params["units"].upper()

    # Image file extension validation
    if not is_valid_image_format(params["imageformat"]):
        raise ParamError(Error.FORMAT + str(params["imageformat"]))
    params["imageformat"] = params["imageformat"].lower()

    return params


def get_signal_from_client(params, webservice):
    """Get timeseries from FDSN client.

    :param params: parameter dictionary with url parameters (network, station, ...)
    :returns: a tuple (trace.time, trace.data, trace.stats)

    """
    start = UTCDateTime(params["start"])
    end = UTCDateTime(params["end"])
    client = get_obspy_client(webservice=webservice)
    st = client.get_waveforms(
        params["network"],
        params["station"],
        params["location"],
        params["channel"],
        start,
        end,
    )
    st.trim(start, end)
    npoints = sum([len(tr.data) for tr in st])
    if npoints > MAX_DATA_POINTS:
        raise ProcessingError(Error.TOO_MUCH_DATA)
    if npoints > MAX_DATA_POINTS_PROCESSING and params["earthunits"]:
        raise ProcessingError(Error.TOO_MUCH_DATA_PROCESSING)
    if len(st) > MAX_PLOTS:
        raise ProcessingError(Error.PLOTS)
    return st


def get_response(params, webservice):
    """Get instrumental response from FDSN client."""
    client = get_obspy_client(webservice=webservice)
    return client.get_stations(
        network=params["network"],
        station=params["station"],
        location=params["location"],
        channel=params["channel"],
        level="response",
        format="xml",
    )


def remove_response(tr, params):
    units = params["units"]
    if params["units"] == "AUTO":
        units = "ACC" if tr.stats.channel[1] == "N" else "VEL"
    water, demean, taper = params["waterlevel"], params["demean"], False
    freqs = params["freqlimits"]
    tr.remove_response(
        output=units,
        water_level=water,
        zero_mean=demean,
        taper=taper,
        pre_filt=freqs,
    )


def get_periodogram(x):
    """Estimate power spectral density of timeseries using a periodogram.

    :param x: timeseries values
    :returns: values in V**2/Hz if x is measured in V and fs is measured in Hz

    """
    f, pxx_den = periodogram(
        x,
        fs=100,  # Sampling frequency of the x timeseries
        window=None,
        nfft=None,
        detrend="constant",
        return_onesided=True,
        scaling="density",  # where Pxx has units of V**2/Hz
    )
    return (f, pxx_den)  # Pxx_den = [10*math.log10(n) for n in Pxx_den] # in dB


def get_bounds(st):
    """Work out the minimum starttime and the maximum endtime.

    :param st: obspy stream
    :returns: a tuple with the bounds for all plots

    """
    min_starttime = min(tr.times("timestamp")[0] for tr in st)
    max_endtime = max(tr.times("timestamp")[-1] for tr in st)
    return (min_starttime, max_endtime)


def date_format_hours(x, pos=None):
    """See date_format_days function
    Only the first tick contains the full date.
    """
    fmt = "%Y-%m-%d %H:%M:%S" if pos == 1 else "%H:%M:%S"
    return datetime.datetime.fromtimestamp(x, tz=datetime.UTC).strftime(fmt)


def date_format_days(x):
    """User-defined function for formatting a tick representing a date.

    :param x: a tick value
    :returns: a string containing the corresponding tick label

    """
    return datetime.datetime.fromtimestamp(x, tz=datetime.UTC).strftime(
        "%Y-%m-%d %H:%M:%S"
    )


def get_units(params, plot_channel):
    units = " counts"
    if params["earthunits"]:
        units = " m/s"
        if params["units"] == "ACC":
            units = " m/s²"
        elif params["units"] == "DISP":
            units = " m"
        elif params["units"] == "AUTO" and (plot_channel[1] == "N" or params["diff"]):
            units = " m/s²"
    return units


def add_scale_bar(params, ax, plot_channel, tick_sz):
    """Builds a scale bar according to the current channel name.

    :param ax: current Axes instance on the current figure
    :param params: parameter dictionary with url parameters (network, station, ...)
    :param plot_channel: current channel name

    """

    scale = (ax.get_ylim()[1] - ax.get_ylim()[0]) / 2
    ax.annotate(
        f"{scale:.2e}" + get_units(params, plot_channel),
        xy=(1.04, 0.50),
        xycoords="axes fraction",
        horizontalalignment="center",
        verticalalignment="center",
        rotation=90,
        fontsize=tick_sz - 1,
    )
    ax.annotate(
        "",
        xy=(1.05, 0.25),
        xytext=(1.05, 0.75),
        xycoords="axes fraction",
        horizontalalignment="center",
        verticalalignment="center",
        arrowprops={"arrowstyle": "-"},
    )


def min_max_downsample(x, y, num_bins):
    # From https://stackoverflow.com/questions/54449631/improve-min-max-downsampling
    # Remaining (last) samples are ignored.
    pts_per_bin = x.size // num_bins

    x_view = x[: pts_per_bin * num_bins].reshape(num_bins, pts_per_bin)
    y_view = y[: pts_per_bin * num_bins].reshape(num_bins, pts_per_bin)
    i_min = np.argmin(y_view, axis=1)
    i_max = np.argmax(y_view, axis=1)

    r_index = np.repeat(np.arange(num_bins), 2)
    c_index = np.sort(np.stack((i_min, i_max), axis=1)).ravel()

    return x_view[r_index, c_index], y_view[r_index, c_index]


def static_plots(params, st):
    """Create static timeseries plots.

    This function return static timeseries plots builds with matplotlib and embedded
    into HTML via Flask renderer.

    :param params: parameter dictionary with url parameters (network, station, ...)
    :param st: obspy stream
    :returns: response_class flask object containing static timeseries plot(s)

    """

    img = io.BytesIO()
    nplots = len(st)
    (left, right) = get_bounds(st)
    margin = (600 / (24 * 60 * 60)) * (right - left)

    tick_sz = params["width"] // 100 - 2
    pcolor = "black" if params["monochrome"] else "#" + params["color"]

    if params["dpi"] != DPI:
        fig = plt.figure(figsize=(12, 4 * nplots), dpi=params["dpi"])
    else:
        width = params["width"] // params["dpi"]
        height = params["height"] // params["dpi"]
        fig = plt.figure(figsize=(width, height * nplots), dpi=params["dpi"])

    for n, tr in enumerate(st):
        n = n + 1
        ax = fig.add_subplot(nplots, 1, n)

        if len(tr) <= MAX_DATA_POINTS_FAST_MODE:
            (x, y) = (tr.times("timestamp"), tr.data)
        else:
            (x, y) = min_max_downsample(tr.times("timestamp"), tr.data, params["width"])

        ax.plot(x, y, color=pcolor, linewidth=0.50)
        ax.grid(color="black", linestyle=":", linewidth=0.25)
        ax.xaxis.set_tick_params(rotation=XTICKS_ROTATION, labelsize=tick_sz)
        ax.xaxis.set_major_locator(MaxNLocator(5))
        ax.yaxis.set_tick_params(labelsize=tick_sz)
        ax.set_xlim(left - margin, right + margin)
        ax.set_ylim(auto=True)

        if not params["spectrum"]:
            day1 = datetime.datetime.fromtimestamp(left, tz=datetime.UTC).day
            day2 = datetime.datetime.fromtimestamp(right, tz=datetime.UTC).day
            if day1 != day2:
                ax.xaxis.set_major_formatter(FuncFormatter(date_format_days))
            else:
                ax.xaxis.set_major_formatter(FuncFormatter(date_format_hours))

        if params["showtitle"]:
            text = (
                tr.stats.network,
                tr.stats.station,
                tr.stats.location,
                tr.stats.channel,
                tr.stats.starttime.isoformat(),
                tr.stats.endtime.isoformat(),
            )
            ax.annotate(
                "{}.{}.{}.{} {} {} UTC".format(*text),
                xy=(0.50, 1.10),
                xycoords="axes fraction",
                horizontalalignment="center",
                verticalalignment="top",
                fontsize=tick_sz,
                weight="bold",
            )

        if params["showscale"]:
            add_scale_bar(params, ax, tr.stats.channel, tick_sz)
        xaxis_name = "Freq" if params["spectrum"] else "Time"
        ax.set_xlabel(xaxis_name, fontsize=tick_sz, labelpad=20)

    plt.tight_layout(h_pad=tick_sz / 3.0)  # Adjust subplot + height padding
    plt.savefig(img, format=params["imageformat"], transparent=False)
    plt.close()
    response = make_response(img.getvalue())
    response.mimetype = "image/" + params["imageformat"]
    return response
