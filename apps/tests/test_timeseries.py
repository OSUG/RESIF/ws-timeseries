# ruff: noqa: S101
import datetime
import re
import sys
import unittest
from copy import copy

import pytest
from obspy import read, read_inventory

sys.path.append("../")

from apps.globals import Error
from apps.timeseries.parameters import Parameters, check_parameters
from apps.utils import (
    ParamError,
    is_valid_bool_string,
    is_valid_channel,
    is_valid_datetime,
    is_valid_float,
    is_valid_image_format,
    is_valid_integer,
    is_valid_location,
    is_valid_network,
    is_valid_station,
)


class MyTest(unittest.TestCase):
    def test_integers(self):
        assert is_valid_integer(1, 0, 1)
        assert is_valid_float(1.9, -5, 5)
        assert not is_valid_integer(-1, 0, 1)
        assert is_valid_integer(0.5, 0, 1)
        assert not is_valid_float(6, -5, 5)

    def test_dates(self):
        assert is_valid_datetime("2018-01-17")
        assert is_valid_datetime("2018-01-17T19:07:01")
        assert not is_valid_datetime("2018-01-17Z19:07:01")
        assert not is_valid_datetime("2018-01-*")
        assert not is_valid_datetime("2018-01-17T19:07:60")
        assert is_valid_datetime("2018-01-17T19:07:01.000021") == datetime.datetime(
            2018,
            1,
            17,
            19,
            7,
            1,
            21,
        )

    def test_networks(self):
        assert is_valid_network("G")  # 1 or 2
        assert is_valid_network("FR")
        assert is_valid_network("F?")
        assert not is_valid_network("FRA")
        assert not is_valid_network("FR?")
        assert not is_valid_network("FR*")
        assert not is_valid_network("F-")

    def test_stations(self):
        assert is_valid_station("C")  # 1 to 5
        assert is_valid_station("CIELZ")
        assert is_valid_station("*")
        assert is_valid_station("CI??")
        assert is_valid_station("CI*")
        assert not is_valid_station("CIELLE")
        assert not is_valid_station("CIELL?")
        assert not is_valid_station("CIçL")

    def test_locations(self):
        assert is_valid_location("01")  # 2
        assert is_valid_location("*")
        assert not is_valid_location("00*")
        assert not is_valid_location("001?")
        assert not is_valid_location("0$")
        assert not is_valid_location("???")

    def test_channels(self):
        assert is_valid_channel("HHZ")  # 3
        assert is_valid_channel("*")
        assert is_valid_channel("H?Z")
        assert is_valid_channel("???")
        assert not is_valid_channel("HHH*")
        assert not is_valid_channel("H$H")

    def test_image_format(self):
        assert is_valid_image_format("png")
        assert is_valid_image_format("jPg")
        assert is_valid_image_format("jpeg")
        assert not is_valid_image_format("pgm")

    def test_bool_string(self):
        assert is_valid_bool_string("yes")
        assert is_valid_bool_string("true")
        assert is_valid_bool_string("t")
        assert is_valid_bool_string("y")
        assert is_valid_bool_string("1")

        assert is_valid_bool_string("yEs")
        assert is_valid_bool_string("TrUe")
        assert is_valid_bool_string("T")
        assert is_valid_bool_string("Y")
        assert is_valid_bool_string("1")

        assert is_valid_bool_string("no")
        assert is_valid_bool_string("false")
        assert is_valid_bool_string("f")
        assert is_valid_bool_string("n")

        assert is_valid_bool_string("nO")
        assert is_valid_bool_string("faLSe")
        assert is_valid_bool_string("F")
        assert is_valid_bool_string("N")

        assert not is_valid_bool_string("fase")
        assert not is_valid_bool_string("O")
        assert not is_valid_bool_string("oui")

    def test_parameters(self):
        p1 = Parameters().todict()
        p1["network"] = "FR"
        p1["station"] = "CIEL"
        p1["location"] = "*"
        p1["channel"] = "HHZ"
        p1["start"] = "2018-02-12T03:08:02"
        p1["end"] = "2018-02-12T04:08:02"
        p1["showtitle"] = "true"
        p1["monochrome"] = "false"
        p1["correct"] = "false"
        p1["earthunits"] = "false"
        p1["imageformat"] = "png"
        p1["spectrum"] = "true"
        p1["color"] = "B22222"
        p1["width"] = 1200
        p1["height"] = 400
        p1["dpi"] = 100
        p1["showscale"] = "true"
        p1["waterlevel"] = None
        p1["freqlimits"] = None
        p1["units"] = "AUTO"
        p1["demean"] = "false"
        p1["detrend"] = "false"
        p1["zerophase"] = "false"
        p1["envelope"] = "false"
        p1["diff"] = "false"
        p1["int"] = "false"
        p1["taper"] = None
        p1["lp"] = None
        p1["hp"] = None
        p1["bp"] = None
        p1["decimate"] = None
        p1["scale"] = None
        p1["divscale"] = None
        p1["nodata"] = "204"
        p1["format"] = "mseed"

        p2 = copy(p1)
        assert check_parameters(p2) == p2

        p2 = copy(p1)
        p2["station"] = "*"
        p2["format"] = "ascii"
        expected = f"{Error.NO_WILDCARDS}station)."
        with pytest.raises(ParamError, match=re.escape(expected)):
            check_parameters(p2)

        p2 = copy(p1)
        p1["start"] = "2018-02-12T03:08:02"
        p1["end"] = "30"
        assert check_parameters(p2) == p2

        p2 = copy(p1)
        p1["start"] = "currentutcday"
        p1["end"] = "60"
        assert check_parameters(p2) == p2

        p2 = copy(p1)
        p2["start"] = None
        expected = f"{Error.MISSING}start"
        with pytest.raises(ParamError, match=re.escape(expected)):
            check_parameters(p2)

        p2 = copy(p1)
        p2["start"] = ""
        expected = f"{Error.TIME}{p2['start']}"
        with pytest.raises(ParamError, match=re.escape(expected)):
            check_parameters(p2)

        p2 = copy(p1)
        p2["start"] = "2018-02-12Z04:08:02"
        expected = f"{Error.TIME}{p2['start']}"
        with pytest.raises(ParamError, match=re.escape(expected)):
            check_parameters(p2)

        p2 = copy(p1)
        p2["width"] = "60"
        expected = f"{Error.WIDTH}{p2['width']}"
        with pytest.raises(ParamError, match=re.escape(expected)):
            check_parameters(p2)

        p2 = copy(p1)
        p2["width"] = "aa"
        expected = f"{Error.WIDTH}{p2['width']}"
        with pytest.raises(ParamError, match=re.escape(expected)):
            check_parameters(p2)

        p2 = copy(p1)
        p2["earthunits"] = "Fals"
        expected = f"Invalid earthunits value: {p2['earthunits']} {Error.BOOL}."
        with pytest.raises(ParamError, match=re.escape(expected)):
            check_parameters(p2)

    def test_processings(self):
        deconvolution = None

        st = read()
        inv = read_inventory()
        deconvolution = st[0].remove_response(inventory=inv)

        assert deconvolution is not None


if __name__ == "__main__":
    unittest.main()
