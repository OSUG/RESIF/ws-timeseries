from tempfile import NamedTemporaryFile

from flask import make_response
from obspy.core import UTCDateTime
from obspy.signal.filter import envelope

from apps.globals import (
    Error,
)
from apps.utils import (
    ProcessingError,
    get_bounds,
    get_obspy_client,
    get_periodogram,
    get_response,
    get_signal_from_client,
    remove_response,
    static_plots,
)

WEBSERVICE = "Timeseries"


def get_processed_signal(st, params):
    """Signal processing"""

    for tr in st.traces:
        for item in params["args"]:
            # Processings are applied only once and in the order given
            # by the list params["args"].
            # To do this with boolean parameters (e.g. demean) we check
            # if they are both true and in the request.
            if params["earthunits"] and item in ("earthunits", "correct"):
                remove_response(tr, params)
            elif params["demean"] and item == "demean":
                tr.detrend("demean")
            elif params["detrend"] and item == "detrend":
                tr.detrend("linear")
            elif params["envelope"] and item == "envelope":
                tr.data = envelope(tr.data)
            elif params["diff"] and item == "diff":
                tr.differentiate(method="gradient")
            elif params["int"] and item == "int":
                tr.integrate(method="cumtrapz")
            elif item == "scale":
                tr.data = params["scale"] * tr.data
            elif item == "divscale":
                tr.data = (1.0 / params["divscale"]) * tr.data
            elif item in ("decimate", "deci"):
                tr.decimate(params["deci"], strict_length=False, no_filter=False)
            elif item == "taper":
                taper_trace(tr, params)
            elif item in ("lpfilter", "lp"):
                tr.filter("lowpass", freq=params["lp"], zerophase=params["zerophase"])
            elif item in ("hpfilter", "hp"):
                tr.filter("highpass", freq=params["hp"], zerophase=params["zerophase"])
            elif item in ("bpfilter", "bp"):
                tr.filter(
                    "bandpass",
                    freqmin=params["bp"][0],
                    freqmax=params["bp"][1],
                    zerophase=params["zerophase"],
                )

    if params["spectrum"]:
        for tr in st.traces:
            tr.time_array, tr.data = get_periodogram(tr.data)

    return st


def taper_trace(tr, params):
    win = "hann" if params["taper"][1] == "HANNING" else params["taper"][1].lower()
    tr.taper(params["taper"][0], type=win, max_length=None, side="both")


def get_file_type(params):
    # (time, values) 2 columns
    if params["format"] in ("ascii", "tspair"):
        file_type, file_ext = "TSPAIR", ".csv"
    # (values) written from left to right (6 columns max)
    elif params["format"] == "slist":
        file_type, file_ext = "SLIST", ".csv"
    elif params["format"] in ("miniseed", "mseed"):
        file_type, file_ext = "MSEED", ".mseed"
    # little-endian SAC
    elif params["format"] == "sac":
        file_type, file_ext = "SAC", ".sac"
    return (file_type, file_ext)


def set_sac_header(st):
    for tr in st:
        stats = tr.stats
        client = get_obspy_client(webservice=WEBSERVICE)
        inventory = client.get_stations(
            network=stats["network"],
            station=stats["station"],
            location=stats["location"],
            channel=stats["channel"],
            level="channel",
        )
        inv_sta = inventory[0][0]
        inv_cha = inv_sta[0]
        stats.sac = {}
        if hasattr(inv_cha, "azimuth"):
            stats.sac["cmpaz"] = inv_cha.azimuth
        if hasattr(inv_cha, "dip"):
            stats.sac["cmpinc"] = inv_cha.dip
        if hasattr(inv_sta, "latitude"):
            stats.sac["stla"] = inv_sta.latitude
        if hasattr(inv_sta, "longitude"):
            stats.sac["stlo"] = inv_sta.longitude
        if hasattr(inv_sta, "depth"):
            stats.sac["stdp"] = inv_sta.depth
        if hasattr(inv_sta, "elevation"):
            stats.sac["stel"] = inv_sta.elevation


def get_file(params, st):
    """Create temporary timeseries file.

    The name is built according to the template :
    resifws-timeseries.2018-11-29T10_11_32.000Z.2018-11-29T23_42_56.000Z
    :param params: parameter dictionary with url parameters (network, station, ...)
    :param st: obspy stream
    :returns: response_class flask object containing the file

    """
    (file_type, file_ext) = get_file_type(params)
    (start, end) = get_bounds(st)

    start = UTCDateTime(start).strftime("%Y%m%dT%H%M%S") + "Z"
    end = UTCDateTime(end).strftime("%Y%m%dT%H%M%S") + "Z"
    period = f"{start}.{end}"

    fname = "resifws-timeseries"
    if len(st) == 1:
        stat = st[0].stats
        fname = f"{stat.network}.{stat.station}.{stat.location}.{stat.channel}"
    fname = fname + "." + period + file_ext

    headers = {"Content-Disposition": "attachment; filename=" + fname}
    tmp = NamedTemporaryFile(delete=True)
    if file_ext == ".sac":
        set_sac_header(st)
    st.write(tmp.name, format=file_type)
    response = make_response((tmp.read(), headers))
    if file_ext == ".csv":
        response.mimetype = "text/csv"
    else:
        response.mimetype = "application/octet-stream"
    tmp.close()
    return response


def get_output(params):
    """Create timeseries plots.

    :param params: parameter dictionary with url parameters (network, station, ...)
    :returns: static plot(s) or data file
    :raises MemoryError raises memory exception
    :raises ValueError raises value exception

    """
    st = get_signal_from_client(params, WEBSERVICE)
    if params["earthunits"]:
        st.attach_response(get_response(params, WEBSERVICE))
    st = get_processed_signal(st, params)

    try:
        if params["format"] == "plot":
            st.merge(method=1)
            response = static_plots(params, st)
        else:
            st.merge(method=1, fill_value=0)
            response = get_file(params, st)
    except (MemoryError, ValueError) as exception:
        raise ProcessingError(Error.PROCESSING) from exception
    else:
        return response
