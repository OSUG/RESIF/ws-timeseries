import re

from flask import request

from apps.globals import (
    DECI_MAX,
    DECI_MIN,
    DPI,
    HEIGHT,
    MAX_DAYS,
    PLOT_COLOR,
    TAPER_WINDOWS,
    WIDTH,
    Error,
)
from apps.utils import (
    ParamError,
    check_base_parameters,
    check_request,
    check_timeseriesplots_parameters,
    is_valid_bpfilter,
    is_valid_integer,
    is_valid_nodata,
    is_valid_output,
    is_valid_taper_window,
)


class Parameters:
    def __init__(self):
        self.network = None
        self.station = None
        self.location = None
        self.channel = None
        self.starttime = None
        self.endtime = None
        self.net = "*"
        self.sta = "*"
        self.loc = "*"
        self.cha = "*"
        self.start = None
        self.end = None
        self.spectrum = "false"
        self.showtitle = "true"
        self.showscale = "true"
        self.monochrome = "false"
        self.width = WIDTH
        self.height = HEIGHT
        self.dpi = DPI
        self.color = PLOT_COLOR
        self.correct = "false"
        self.earthunits = "false"
        self.demean = "false"
        self.waterlevel = None
        self.freqlimits = None
        self.units = "AUTO"
        self.imageformat = "png"
        self.taper = None
        self.lpfilter = None
        self.hpfilter = None
        self.bpfilter = None
        self.lp = None
        self.hp = None
        self.bp = None
        self.decimate = None
        self.deci = None
        self.detrend = "false"
        self.zerophase = "false"
        self.envelope = "false"
        self.diff = "false"
        self.int = "false"
        self.scale = None
        self.divscale = None
        self.format = None
        self.nodata = "204"
        self.constraints = {
            "alias": [
                ("network", "net"),
                ("station", "sta"),
                ("location", "loc"),
                ("channel", "cha"),
                ("starttime", "start"),
                ("endtime", "end"),
                ("lpfilter", "lp"),
                ("hpfilter", "hp"),
                ("bpfilter", "bp"),
                ("decimate", "deci"),
                ("earthunits", "correct"),
            ],
            "booleans": [
                "spectrum",
                "showtitle",
                "showscale",
                "monochrome",
                "correct",
                "earthunits",
                "demean",
                "detrend",
                "zerophase",
                "envelope",
                "diff",
                "int",
            ],
            "floats": ["lp", "hp", "scale", "divscale"],
            "not_none": ["start", "end", "format"],
        }

    def todict(self):
        return self.__dict__


def check_parameters(params):
    params = check_base_parameters(params, MAX_DAYS)
    params = check_timeseriesplots_parameters(params)

    # scale parameter
    if params["scale"] is not None and params["divscale"] is not None:
        raise ParamError(Error.SCALE_DIVSCALE)

    # decimation parameter validation
    if params["deci"] is not None:
        if not is_valid_integer(params["deci"], DECI_MIN, DECI_MAX):
            raise ParamError(Error.DECI + str(params["deci"]))
        params["deci"] = int(params["deci"])

    # filter parameter validations
    if params["taper"] is not None:
        params["taper"] = tuple(params["taper"].split(","))
        if not is_valid_taper_window(params["taper"]):
            raise ParamError(Error.TAPER + str(params["taper"]))
        if len(params["taper"]) == 1:
            params["taper"] = (float(params["taper"][0]), TAPER_WINDOWS[0])
        else:
            params["taper"] = (
                float(params["taper"][0]),
                params["taper"][1].upper(),
            )

    if params["bp"] is not None:
        params["bp"] = tuple(re.split("[/,;-]", params["bp"]))
        if not is_valid_bpfilter(params["bp"]):
            raise ParamError(Error.BP + str(params["bp"]))
        params["bp"] = (float(params["bp"][0]), float(params["bp"][1]))

    # output parameter validation
    if not is_valid_output(params["format"]):
        raise ParamError(Error.OUTPUT_TIMESERIES + str(params["format"]))
    params["format"] = params["format"].lower()

    # nodata parameter validation
    if not is_valid_nodata(params["nodata"]):
        raise ParamError(Error.NODATA_CODE + str(params["nodata"]))
    params["nodata"] = params["nodata"].lower()

    # wildcards or list are allowed only with plot and mseed output options
    if params["format"] not in ("plot", "mseed", "miniseed"):
        for key in ("network", "station", "location", "channel"):
            if re.search(r"[,*?]", params[key]):
                raise ParamError(Error.NO_WILDCARDS + key + ").")

    return params


def checks_get():
    # get default parameters
    params = Parameters().todict()

    # check if the parameters are unknown
    check_request(params)
    params["args"] = tuple(request.args)
    return check_parameters(params)
