#############################################
FROM ghcr.io/astral-sh/uv:python3.12-bookworm-slim AS timeseries-builder

# Enable bytecode compilation
# Copy from the cache instead of linking since it's a mounted volume
ENV UV_COMPILE_BYTECODE=1 \
    UV_LINK_MODE=copy

WORKDIR /app
COPY pyproject.toml uv.lock ./
# Sync only project dependencies
RUN --mount=type=cache,target=/root/.cache/uv \
    uv sync --frozen --no-install-project --no-dev

#############################################
FROM python:3.12-slim AS ws-timeseries

RUN addgroup --system app && \
    adduser --system --group app
USER app
WORKDIR /app
COPY --from=timeseries-builder --chown=app:app /app /app
COPY . .

ARG CI_COMMIT_SHORT_SHA
ENV COMMIT=$CI_COMMIT_SHORT_SHA \
    SENTRY_RELEASE=$CI_COMMIT_SHORT_SHA \
    PATH="/app/.venv/bin:$PATH"

# ENTRYPOINT ["gunicorn", "--bind", "0.0.0.0:8000", "timeseries:app"]
