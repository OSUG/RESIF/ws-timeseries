var clipboard;
var filters = $("#list-filters").sortable({
  onChange: function (evt) {
    generate_url();
  },
});

input_or_select = function (key) {
  let item_input = $("input[name=" + key + "]");
  let item_select = $("select[name=" + key + "]");

  if (item_input.length) {
    return item_input;
  } else {
    return item_select;
  }
};

toggle = function (key, subkeys) {
  let item_check = $("#toggle-" + key);

  if (subkeys) {
    subkeys.forEach(function (subkey) {
      let item_value = input_or_select(key + "_" + subkey);
      item_value.prop("disabled", !item_check.prop("checked"));
    });
  } else {
    let item_value = input_or_select(key);
    item_value.prop("disabled", !item_check.prop("checked"));
  }
};

merge_datetimes = function (searchParams) {
  for (let key of ["start", "end"]) {
    let date = searchParams.get("date_" + key);
    if (date) {
      let time = searchParams.get("time_" + key);
      if (time) {
        searchParams.set(key, date + "T" + time);
      } else {
        searchParams.set(key, date);
      }
    }

    if (searchParams.has("date_" + key)) {
      searchParams.delete("date_" + key);
    }
    if (searchParams.has("time_" + key)) {
      searchParams.delete("time_" + key);
    }
  }

  if (searchParams.has("start") && !searchParams.has("end")) {
    searchParams.set("end", "currentutcday");
  }

  return searchParams;
};

merge_subinputs = function (searchParams, key, subkeys, separator = "-") {
  let values = [];
  subkeys.forEach(function (subkey) {
    let item = key + "_" + subkey;
    if (searchParams.has(item)) {
      let value = searchParams.get(item);
      values.push(value);
      searchParams.delete(item);
    }
  });
  if (values.length === subkeys.length) {
    searchParams.set(key, values.join(separator));
  }
  return searchParams;
};

generate_url = function () {
  let searchParams = new URLSearchParams();

  let form = $("#form-url-builder");
  let queryUrl = form.attr("data-url");
  let formData = form.serializeArray();
  formData.forEach(function (item) {
    if (item.value !== "") {
      searchParams.append(item.name, item.value);
    }
  });

  searchParams = merge_datetimes(searchParams);
  searchParams = merge_subinputs(searchParams, "freqlimits", [
    "f1",
    "f2",
    "f3",
    "f4",
  ]);

  let urlParams = new URLSearchParams();
  for (let key of [
    "network",
    "station",
    "location",
    "channel",
    "start",
    "end",
    "nodata",
  ]) {
    if (searchParams.has(key)) {
      urlParams.append(key, searchParams.get(key));
      searchParams.delete(key);
    }
  }

  for (let key of filters.sortable("toArray")) {
    if (searchParams.has(key)) {
      urlParams.append(key, searchParams.get(key));
      searchParams.delete(key);
    }
  }
  for (let param of searchParams.entries()) {
    urlParams.append(param[0], param[1]);
  }

  let url = queryUrl + urlParams.toString();
  $("#input-generated-url").val(url);
  clipboard = new ClipboardJS("#button-copy");

  return url;
};

run_url = function () {
  let url = generate_url();
  window.open(url, "_blank").focus();
};
