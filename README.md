# Timeseries / TimeseriesPlot
## About
This software provides access to the timeseries data of the RESIF seismic network. Optional signal processing may be applied and data may be returned in several formats.

## Installation

Install https://docs.astral.sh/uv/getting-started/installation/[uv]. Uv is a tool for dependency management and packaging python applications.
```shell
curl -LsSf https://astral.sh/uv/install.sh | sh
echo 'eval "$(uv generate-shell-completion bash)"' >> ~/.bashrc
echo 'eval "$(uvx --generate-shell-completion bash)"' >> ~/.bashrc
```

Then, clone the project from the Gitlab repository:
```shell
git clone git@gricad-gitlab.univ-grenoble-alpes.fr:OSUG/RESIF/ws-timeseries.git
cd ws-timeseries
```

Install python dependencies :
```shell
uv sync --frozen

# show the dependency tree for the project
uv tree
```

## Usage
### Local server
In order to launch timeseries/timeseriesplot in your browser :

```shell
uv run flask --app timeseries run --debug
```

or 

```shell
uv run flask --app timeseriesplot run --debug
```

The application will be available at [http://localhost:5000/](http://localhost:5000/)

## Buildah/Podman
Once buildah and podman installed, you can build the waveqc docker image with the following command
```shell
buildah bud -t timeseries --layers --build-arg COMMIT=123456789 --network=host
```

... and run it with the following command

```shell
podman run -p 8000:8000 --rm timeseries  sh -c "gunicorn --bind :8000 'timeseriesplot:app'"
```

The application will be available at [http://localhost:8000/](http://localhost:8000/)

## Support
For questions about accessing data, filling bug reports, making suggestions, asking for help, please visit [our helpdesk](https://gitlab.com/resif/sismo-help) or send an email to sismo-help@resif.fr

## Contributing
If you're willing to contribute to this project, here are some tools that we are using to lint, test and format code.

### Pre-commit hooks
First of, install the [pre-commit](https://pre-commit.com/#usage) hooks.

To do so, activate the virtual env with the following command and run the install command for pre-commit:
```shell
uv run pre-commit install
```
Now, each time you'll want to commit, several tasks will be run to ensure certain standards of code quality:

- [ruff](https://docs.astral.sh/ruff/) : a linter and formatter for python files

Configuration for pre-commit is located in `.pre-commit-config.yaml` file

### Gitlab Ci jobs
In addition to pre-commit hooks, the following jobs are run after each push :

- [ruff](https://docs.astral.sh/ruff/) : linting
- [pytest](https://docs.pytest.org) : unit tests

Each of these tools can be run locally with the following commands : `uv run pytest` and `ruff check`

### Tooling configuration
If you are curious about the configuration of all of these tools, please consult the `pyproject.toml` file

## Authors and acknowledgment
* Jérôme Touvier (main developer - not a Résif-DC member anymore)
* Philippe Bollard (contributor)
* Simon Panay (contributor)

## License
This software is licenced under the [GPLv3](https://choosealicense.com/licenses/gpl-3.0/) Licence

## Project status
Production/Stable

***
