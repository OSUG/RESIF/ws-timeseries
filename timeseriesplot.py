import os
from logging.config import dictConfig

from flask import Flask, make_response, render_template, request
from obspy.clients.fdsn.client import (
    FDSNBadGatewayException,
    FDSNBadRequestException,
    FDSNException,
    FDSNForbiddenException,
    FDSNInternalServerException,
    FDSNNoDataException,
    FDSNNotImplementedException,
    FDSNRequestTooLargeException,
    FDSNServiceUnavailableException,
    FDSNTooManyRequestsException,
    FDSNUnauthorizedException,
)
from werkzeug.middleware.proxy_fix import ProxyFix

from apps.globals import VERSION
from apps.timeseriesplot.output import get_output
from apps.timeseriesplot.parameters import checks_get
from apps.utils import (
    ParamError,
    ProcessingError,
    handle_exception,
    initialize_sentry,
)

dictConfig(
    {
        "version": 1,
        "formatters": {
            "default": {
                "format": (
                    "[%(asctime)s] %(levelname)s [%(module)s:%(lineno)d]"
                    "[%(funcName)s] %(message)s"
                ),
            }
        },
        "handlers": {
            "wsgi": {
                "class": "logging.StreamHandler",
                "stream": "ext://flask.logging.wsgi_errors_stream",
                "formatter": "default",
            }
        },
        "root": {"level": "INFO", "handlers": ["wsgi"]},
    }
)

initialize_sentry()
app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app, x_prefix=1)


@app.errorhandler(FDSNNoDataException)
@app.errorhandler(FDSNBadRequestException)
@app.errorhandler(FDSNUnauthorizedException)
@app.errorhandler(FDSNForbiddenException)
@app.errorhandler(FDSNTooManyRequestsException)
@app.errorhandler(FDSNNotImplementedException)
@app.errorhandler(FDSNBadGatewayException)
@app.errorhandler(FDSNServiceUnavailableException)
@app.errorhandler(FDSNRequestTooLargeException)
@app.errorhandler(FDSNInternalServerException)
@app.errorhandler(FDSNException)
@app.errorhandler(ProcessingError)
@app.errorhandler(ParamError)
def error_handling(exception):
    return handle_exception(exception, request)


@app.after_request
def log_request_info(response):
    if "healthcheck" not in request.path:
        app.logger.info(
            "%s %s%s %s",
            request.method,
            request.root_path,
            request.full_path,
            response.status,
        )
    return response


@app.route("/query")
def query():
    params = checks_get()
    return get_output(params)


@app.route("/application.wadl")
def wadl():
    template = render_template("timeseriesplot/wadl.jinja2")
    response = make_response(template)
    response.headers["Content-Type"] = "application/xml"
    return response


@app.route("/version", strict_slashes=False)
def version():
    response = make_response(VERSION)
    response.headers["Content-Type"] = "text/plain"
    return response


@app.route("/commit", strict_slashes=False)
def commit():
    response = make_response(os.environ.get("COMMIT", "unspecified"))
    response.headers["Content-Type"] = "text/plain"
    return response


@app.route("/healthcheck", strict_slashes=False)
def healthcheck():
    response = make_response("OK")
    response.headers["Content-Type"] = "text/plain"
    return response


@app.route("/", strict_slashes=False)
@app.route("/local=<locale>")
def doc(locale="fr"):
    locale = "en" if locale not in ("fr", "en") else locale
    return render_template(f"timeseriesplot/{locale}.jinja2")
